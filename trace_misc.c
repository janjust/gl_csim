/* Some misc. functions for manipulating counters and keeping track
 * of variable info 
 */
#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "d4.h"

static void vupdate(d4vtype* vtmp, const unsigned int setnumber)
{
  vtmp->trefs++;
  vtmp->tmptrefs++;
  vtmp->sets[setnumber].refs++;
  vtmp->sets[setnumber].tmprefs++;
}

static d4vtype* vnew(const d4memref* m, const unsigned int setnumber, const int numsets)
{
	d4vtype* vtmp;

  vtmp = malloc(sizeof(d4vtype));
	vtmp->sets = calloc(numsets, sizeof(d4setstype));
  
	strcpy(vtmp->vname, m->vname);
  strcpy(vtmp->scope, m->scope);
  vtmp->trefs = 1;
  vtmp->tmptrefs = 1;
  vtmp->tmisses = 0;
  vtmp->tmptmisses = 0;
  vtmp->sets[setnumber].refs = 1;
  vtmp->sets[setnumber].tmprefs = 1;
  vtmp->sets[setnumber].misses = 0;
  vtmp->sets[setnumber].tmpmisses = 0;
  vtmp->next = NULL;
  
  return vtmp;
}

d4vtype* update_vcntr(d4ftype* ftmp, const d4memref* m, 
										const unsigned int setnumber, unsigned int numsets)
{
  d4vtype* vtmp = ftmp->variable;
  while(vtmp->next != NULL){
    if(strcmp(vtmp->vname, m->vname) == 0){
			if(vtmp->tmptrefs == 0){
				ftmp->tmptvars++;
			}
      vupdate(vtmp, setnumber);
      return vtmp;
    }
    vtmp = vtmp->next;
  }
  if(strcmp(vtmp->vname, m->vname) == 0){
			if(vtmp->tmptrefs == 0){
				ftmp->tmptvars++;
			}
      vupdate(vtmp, setnumber);
    return vtmp;
  }

  /* set new var */
  vtmp->next = vnew(m, setnumber, numsets);
	vtmp = vtmp->next;
	ftmp->tvars++;
	ftmp->tmptvars++;
  return vtmp;
}

d4vtype*  init_var(const d4memref* m, const unsigned int setnumber, unsigned int numsets)
{
  d4vtype* vtmp = malloc(sizeof(d4vtype));
	vtmp->sets = calloc(numsets, sizeof(d4setstype));
  
	strcpy(vtmp->vname, m->vname);
  strcpy(vtmp->scope, m->scope);
  vtmp->trefs = 1;
  vtmp->tmptrefs = 1;
  vtmp->tmisses = 0;
  vtmp->tmptmisses = 0;
  vtmp->sets[setnumber].refs = 1;
  vtmp->sets[setnumber].tmprefs = 1;
  vtmp->sets[setnumber].misses = 0;
  vtmp->sets[setnumber].tmpmisses = 0;
	vtmp->next = NULL;
  return vtmp;
}

static d4ftype* fnew(const d4memref* m, const unsigned int setnumber, unsigned int numsets)
{
	d4ftype* ftmp; 
  
	ftmp = malloc(sizeof(d4ftype));
  ftmp->sets = calloc(numsets, sizeof(d4setstype));
	

  strcpy(ftmp->fname, m->fname);
  ftmp->tvars = 0;
  ftmp->tmptvars = 0;
  ftmp->trefs = 1;
  ftmp->tmptrefs = 1;
  ftmp->tmisses = 0;
  ftmp->tmptmisses = 0;
  ftmp->sets[setnumber].refs = 1;
  ftmp->sets[setnumber].tmprefs = 1;
  ftmp->sets[setnumber].misses = 0;
  ftmp->sets[setnumber].tmpmisses = 0;
	ftmp->next = NULL;
  ftmp->variable = NULL;
	
  return ftmp;
}
  
static void fupdate(d4ftype* ftmp, const d4memref* m, const unsigned int setnumber)
{
  ftmp->trefs++;
  ftmp->tmptrefs++;
	ftmp->sets[setnumber].refs++;
	ftmp->sets[setnumber].tmprefs++;
}         
 
d4ftype* update_fcntr(d4ftype* ftmp, const d4memref* m, 
                                  const unsigned int setnumber, unsigned int numsets,
                                  d4mltype* mlist)
{
  while(ftmp->next != NULL){
    if(strcmp(ftmp->fname, m->fname) == 0){
      if(ftmp->tmptrefs == 0){
				mlist->tmptfuncs++;
			}
			fupdate(ftmp, m, setnumber);
      return ftmp;
    }
    ftmp = ftmp->next;
  }
  if(strcmp(ftmp->fname, m->fname) == 0){
      if(ftmp->tmptrefs == 0){
				mlist->tmptfuncs++;
			}
	    fupdate(ftmp, m, setnumber);
    return ftmp;
  }
    
  /* must be new */
  ftmp->next = fnew(m, setnumber, numsets);
	ftmp = ftmp->next;
  mlist->tfuncs++;
  mlist->tmptfuncs++;
  return ftmp;
}

d4ftype* update_fsegs(d4ftype* ftmp, char segm, const unsigned int setnumber)
{
  /*search for segment and update */
  /* Update stack, global, heap*/
  switch(segm){
    case 'S':
      ftmp->trefs++;
      ftmp->tmptrefs++;
      ftmp->sets[setnumber].refs++;
      ftmp->sets[setnumber].tmprefs++;
      ftmp = ftmp->next;
      ftmp = ftmp->next;
			ftmp = ftmp->next;
      break;
    case 'G':
      ftmp = ftmp->next;
      ftmp->trefs++;
      ftmp->tmptrefs++;
      ftmp->sets[setnumber].refs++;
      ftmp->sets[setnumber].tmprefs++;
      ftmp = ftmp->next;
			ftmp = ftmp->next;
      break;
    case 'H':
      ftmp = ftmp->next;
      ftmp = ftmp->next;
      ftmp->trefs++;
      ftmp->tmptrefs++;
      ftmp->sets[setnumber].refs++;
      ftmp->sets[setnumber].tmprefs++;
      ftmp = ftmp->next;
			break;
		case 'N':
			ftmp = ftmp->next;
			ftmp = ftmp->next;
			ftmp = ftmp->next;
			break;
  }

  return ftmp;
}

d4ftype* init_functions(d4ftype** retftmp, d4vtype** vtmp, const d4memref* m, const unsigned int setnumber, const unsigned int numsets)
{
  d4ftype* ret_;
  d4ftype* ftmp;

  /* below are per function stats */
  /* Add _STACK_ */
  ftmp = malloc(sizeof(d4ftype)); /* _STACK_ */
  ret_ = ftmp;
  ftmp->variable = NULL;
  ftmp->sets = calloc(numsets, sizeof(d4setstype));
	/* set _STACK_ */
  strcpy(ftmp->fname, "_STACK_");
  ftmp->tvars = 0;
	ftmp->tmptvars = 0;
  ftmp->trefs = 0;
	ftmp->tmptrefs = 0;
  ftmp->tmisses = 0;
	ftmp->tmptmisses = 0;
  ftmp->sets[setnumber].refs = 0;
	ftmp->sets[setnumber].tmprefs = 0;
  ftmp->sets[setnumber].misses = 0;
	ftmp->sets[setnumber].tmpmisses = 0;
	if(m->seg == 'S') {
      ftmp->trefs=1;
      ftmp->sets[setnumber].refs = 1;
      ftmp->tmptrefs=1;
      ftmp->sets[setnumber].tmprefs = 1;
  }
 
  /* Add _GLOBAL_ */	
  ftmp->next = malloc(sizeof(d4ftype)); /* _GLOBAL_ */
  ftmp = ftmp->next;
  ftmp->sets = calloc(numsets, sizeof(d4setstype));
  ftmp->variable = NULL;
  /* set _GLOBAL_ */
  strcpy(ftmp->fname, "_GLOBAL_");
  ftmp->tvars = 0;
	ftmp->tmptvars = 0;
  ftmp->trefs = 0;
	ftmp->tmptrefs = 0;
  ftmp->tmisses = 0;
	ftmp->tmptmisses = 0;
  ftmp->sets[setnumber].refs = 0;
	ftmp->sets[setnumber].tmprefs = 0;
  ftmp->sets[setnumber].misses = 0;
	ftmp->sets[setnumber].tmpmisses = 0;
  if(m->seg == 'G') {
      ftmp->trefs=1;
			ftmp->tmptrefs = 1;
      ftmp->sets[setnumber].refs = 1;
			ftmp->sets[setnumber].tmprefs = 1;
  }
  
  /* Add _HEAP_ */	
  ftmp->next = malloc(sizeof(d4ftype)); /* _HEAP_ */
  ftmp = ftmp->next;
  ftmp->sets = calloc(numsets, sizeof(d4setstype));
  ftmp->variable = NULL;
  /* set _HEAP_ */
  strcpy(ftmp->fname, "_HEAP_");
  ftmp->tvars = 0;
	ftmp->tmptvars = 0;
  ftmp->trefs = 0;
	ftmp->tmptrefs = 0;
  ftmp->tmisses = 0;
	ftmp->tmptmisses = 0;
  ftmp->sets[setnumber].refs = 0;
	ftmp->sets[setnumber].tmprefs = 0;
  ftmp->sets[setnumber].misses = 0;
	ftmp->sets[setnumber].tmpmisses = 0;
  if(m->seg == 'H'){
      ftmp->trefs = 1;
			ftmp->tmptrefs = 1;
      ftmp->sets[setnumber].refs = 1;
			ftmp->sets[setnumber].tmprefs = 1;
  }
 
  // new function
  ftmp->next = malloc(sizeof(d4ftype)); // new function
  ftmp = ftmp->next;	
  ftmp->sets = calloc(numsets, sizeof(d4setstype));	
  ftmp->next = NULL;
  ftmp->variable = NULL; // will set that below	
  
  // new variable
  *vtmp = malloc(sizeof(d4vtype));
  ftmp->variable = *(vtmp);
  (*vtmp)->sets = calloc(numsets, sizeof(d4setstype));
  (*vtmp)->next = NULL;
  
  // set up function
  strcpy(ftmp->fname, m->fname);
  ftmp->tvars = 1;
	ftmp->tmptvars = 1;
  ftmp->trefs = 1;
	ftmp->tmptrefs = 1;
	ftmp->tmisses = 0;
	ftmp->tmptmisses = 0;
  ftmp->sets[setnumber].refs = 1;
	ftmp->sets[setnumber].tmprefs = 1;
  ftmp->sets[setnumber].misses = 0;
	ftmp->sets[setnumber].tmpmisses = 0;
  
	// set up variable 
  strcpy((*vtmp)->vname, m->vname);
  strcpy((*vtmp)->scope, m->scope);
  (*vtmp)->trefs = 1;
	(*vtmp)->tmptrefs = 1;
  (*vtmp)->tmisses = 0;
	(*vtmp)->tmptmisses = 0;
  (*vtmp)->sets[setnumber].refs = 1;
	(*vtmp)->sets[setnumber].tmprefs = 1;
  (*vtmp)->sets[setnumber].misses = 0;
	(*vtmp)->sets[setnumber].tmpmisses = 0;
  //(*vtmp)->fnc_evictors = NULL;
	
	(*retftmp) = ftmp;
  return ret_;
}

void update_segments(char seg, d4segstype* seginfo)
{
  switch(seg){
    case 'S':
      seginfo->stack[0]++;
      break;
    case 'H':
      seginfo->heap[0]++;
      break;
    case 'G':
      seginfo->global[0]++;
      break;
    case 'N':
      break; /*do nothing, this may be a non op, after a copy back or inv.*/
    default:
      printf("Error: trace_misc->update_segments: no segment info?\n");
      exit(0);
  }
  return;
}

static d4stype2* moreblocks(d4stype1* sitmp, const int numsets, unsigned int h_enum)
{
  
	int i=0, j=0;	
	unsigned int newbsize = (h_enum - sitmp->bcnts) + 100;

	sitmp->blocks = realloc(sitmp->blocks, (sitmp->bcnts + newbsize) * sizeof(d4stype2));
	
	for(i=sitmp->bcnts; i<sitmp->bcnts+newbsize; i++){
    sitmp->blocks[i].sets = calloc(numsets, sizeof(d4setstype));
		for(j=0; j<numsets; j++){
			sitmp->blocks[i].sets[j].refs = 0;
			sitmp->blocks[i].sets[j].tmprefs = 0;
			sitmp->blocks[i].sets[j].misses = 0;
			sitmp->blocks[i].sets[j].tmpmisses = 0;
		}
	  sitmp->blocks[i].trefs = 0;
	  sitmp->blocks[i].tmptrefs = 0;
    sitmp->blocks[i].tmisses = 0;
    sitmp->blocks[i].tmptmisses = 0;
	}
  sitmp->bcnts += newbsize;
  sitmp->tmpbcnts += newbsize;
  return sitmp->blocks;
}

d4stype1* update_structinfo(d4mltype* mltmp, const d4memref* m,
                            const unsigned int setnumber, unsigned int numsets)
{
  int i=0, j=0;
	d4stype1* sitmp = mltmp->nexts;
  while(sitmp->next != NULL){
    if(strcmp(sitmp->sname, m->vname) == 0){
      if(m->h_enum >= sitmp->bcnts){
        sitmp->blocks = moreblocks(sitmp, numsets, m->h_enum);
      }
			if(sitmp->blocks[m->h_enum].trefs == 0){
				sitmp->curbcnts++;
			}
			if(sitmp->blocks[m->h_enum].tmptrefs == 0){	
				sitmp->tmpcurbcnts++;
			}

			sitmp->ttrefs++;
			sitmp->tmpttrefs++;
	    sitmp->blocks[m->h_enum].trefs++;
	    sitmp->blocks[m->h_enum].tmptrefs++;
      sitmp->blocks[m->h_enum].sets[setnumber].refs++;
      sitmp->blocks[m->h_enum].sets[setnumber].tmprefs++;
      return sitmp;
    }
    sitmp = sitmp->next;
  }
 
	if(strcmp(sitmp->sname, m->vname) == 0){
		if(m->h_enum >= sitmp->bcnts){
			sitmp->blocks = moreblocks(sitmp, numsets, m->h_enum); 
		}
		if(sitmp->blocks[m->h_enum].trefs == 0){
			sitmp->curbcnts++;
		}
		if(sitmp->blocks[m->h_enum].tmptrefs == 0){
			sitmp->tmpcurbcnts++;
		}
		sitmp->ttrefs++;
		sitmp->tmpttrefs++;
		sitmp->blocks[m->h_enum].trefs++;
		sitmp->blocks[m->h_enum].tmptrefs++;
		sitmp->blocks[m->h_enum].sets[setnumber].refs++;
		sitmp->blocks[m->h_enum].sets[setnumber].tmprefs++;
		return sitmp;
  }

  /* must be new */
	mltmp->tstructs++;
	mltmp->tmptstructs++;
	sitmp->next = malloc(sizeof(d4stype1));
	sitmp = sitmp->next;
	strcpy(sitmp->sname,m->vname);
	sitmp->blocks = calloc(m->h_enum+100, sizeof(d4stype2));
	sitmp->bcnts = m->h_enum+100;
	sitmp->tmpbcnts = m->h_enum+100;

	for(i = 0; i < sitmp->bcnts; i++){
		sitmp->blocks[i].sets = calloc(numsets, sizeof(d4setstype));
		for(j = 0; j < numsets; j++){
			sitmp->blocks[i].sets[j].refs = 0;
			sitmp->blocks[i].sets[j].tmprefs = 0;
			sitmp->blocks[i].sets[j].misses = 0;
			sitmp->blocks[i].sets[j].tmpmisses = 0;
		}
    sitmp->blocks[i].trefs = 0;
    sitmp->blocks[i].tmptrefs = 0;
    sitmp->blocks[i].tmisses = 0;
    sitmp->blocks[i].tmptmisses = 0;
	}
	sitmp->curbcnts = 1;
	sitmp->tmpcurbcnts = 1;
	sitmp->ttrefs = 1;
	sitmp->tmpttrefs = 1;
	sitmp->ttmisses = 0;
	sitmp->tmpttmisses = 0;
	sitmp->blocks[m->h_enum].trefs = 1;
	sitmp->blocks[m->h_enum].tmptrefs = 1;
	sitmp->blocks[m->h_enum].sets[setnumber].refs = 1;
	sitmp->blocks[m->h_enum].sets[setnumber].tmprefs = 1;
  sitmp->blocks[m->h_enum].sets[setnumber].misses = 0;
  sitmp->blocks[m->h_enum].sets[setnumber].tmpmisses = 0;
	sitmp->next = NULL;
  return sitmp;
}

d4stype1* init_structinfo(const d4memref* m, unsigned int setnumber, unsigned int numsets)
{
  int i=0, j=0;
  d4stype1* sitmp = malloc(sizeof(d4stype1));
 
	//printf("init_structinfo(beg); <%s>\n", m->vname);
  strcpy(sitmp->sname, m->vname);
  sitmp->blocks = calloc(m->h_enum+100, sizeof(d4stype2));
	sitmp->bcnts = m->h_enum+100;
	sitmp->tmpbcnts = m->h_enum+100;
 
	for(i=0; i<m->h_enum+100; i++){
    sitmp->blocks[i].sets = calloc(numsets, sizeof(d4setstype));
		for(j=0; j<numsets; j++){
			sitmp->blocks[i].sets[j].refs = 0;
			sitmp->blocks[i].sets[j].tmprefs = 0;
			sitmp->blocks[i].sets[j].misses = 0;
			sitmp->blocks[i].sets[j].tmpmisses = 0;
		}
	  sitmp->blocks[i].trefs = 0;
	  sitmp->blocks[i].tmptrefs = 0;
    sitmp->blocks[i].tmisses = 0;
    sitmp->blocks[i].tmptmisses = 0;
	}
	
	sitmp->curbcnts=1;
	sitmp->tmpcurbcnts=1;
	sitmp->ttrefs=1;
	sitmp->tmpttrefs=1;
	sitmp->ttmisses=0;
	sitmp->tmpttmisses=0;
	sitmp->blocks[m->h_enum].trefs++;
	sitmp->blocks[m->h_enum].tmptrefs++;
	sitmp->blocks[m->h_enum].sets[setnumber].refs++;
	sitmp->blocks[m->h_enum].sets[setnumber].tmprefs++;
	sitmp->next = NULL;
	
  return sitmp;
}

/* update conflicts */
//void update_conflicts(d4cache* c, d4stacknode* rptr,
//                      d4ftype* fin, d4vtype* vin)
//{
	//printf("evictor: fin->function %s, vin->variable %s\n", fin->function, vin->variable);
	
	//int i = 0, fnc_set=0, var_set=0;
	//d4ftype* fout;
	//d4vtype* vout;
	//evictor_ftype* tmp_f_evictor;
	//evictor_vtype* tmp_v_evictor;
	
//	for(i=0; i<D4LG2MASK(D4VAL(c, lg2blocksize))+1; i++){
//		//fout = rptr->fncptr[i];
//		//vout = rptr->varptr[i];
//	  //printf("%d: fout->function %s, vout->variable %s\n",i, fout->function, vout->variable);
//	
//		if(vout!=NULL){
//			if(vout->fnc_evictors != NULL){
//        
//				/* find the function */
//        tmp_f_evictor = vout->fnc_evictors;
//				while(!fnc_set){
//          
//					//printf("tmp_f_evictor->function %s\n", tmp_f_evictor->function);
//					if(strcmp(tmp_f_evictor->function,fin->function)==0){
//						//printf("found fnc_evictor %s cmp_to(%s)\n", tmp_f_evictor->function, fin->function);
//            tmp_f_evictor->num_f_evictions++;
//           	fnc_set=1;
//						
//						//printf("\tv_evictor->variable %s\n", tmp_v_evictor->variable);
//						/* check the variable */
//            tmp_v_evictor = tmp_f_evictor->next_v;
//						if(tmp_v_evictor!=NULL){
//              /*search and update*/
//              while(!var_set){
//                if(strcmp(tmp_v_evictor->variable,vin->variable)==0){
//									//printf("found var_evictor %s cmp_to(%s)\n", tmp_v_evictor->variable, vin->variable);
//                  tmp_v_evictor->num_v_evictions++;
//                  var_set=1;
//                }
//								else{
//									if(tmp_v_evictor->next == NULL){
//										/*insert new v_evictor*/
//										tmp_v_evictor->next = malloc(sizeof(evictor_vtype));
//										tmp_v_evictor = tmp_v_evictor->next;
//										strcpy(tmp_v_evictor->variable,vin->variable);
//										tmp_v_evictor->num_v_evictions = 1;
//										tmp_v_evictor->next=NULL;
//										var_set=1;
//										//printf("evicting: %s %s \n", fout->function, vout->variable);
//							  	}
//									else
//                  	tmp_v_evictor = tmp_v_evictor->next;
//								}
//							}
//            }
//            else{
//              /* insert new v_evictor */
//              tmp_f_evictor->next_v = malloc(sizeof(evictor_vtype));
//              tmp_v_evictor = tmp_f_evictor->next_v;
//							strcpy(tmp_v_evictor->variable, vin->variable);
//              tmp_v_evictor->num_v_evictions=1;
//              tmp_v_evictor->next = NULL;
//							var_set=1;
//            }
//          }
//					else{
//						if(tmp_f_evictor->next == NULL){
//							fnc_set=1;
//							/* insert new function evictor*/
//							tmp_f_evictor->next = malloc(sizeof(evictor_ftype));
//							tmp_f_evictor = tmp_f_evictor->next;
//							strcpy(tmp_f_evictor->function, fin->function);
//							tmp_f_evictor->num_f_evictions=1;
//							tmp_f_evictor->next = NULL;
//							
//							/* insert a new variable evictor */
//							tmp_f_evictor->next_v = malloc(sizeof(evictor_vtype));
//							tmp_v_evictor=tmp_f_evictor->next_v;
//							strcpy(tmp_v_evictor->variable, vin->variable);
//							tmp_v_evictor->num_v_evictions=1;
//							tmp_v_evictor->next = NULL;
//							var_set=1;
//							//printf("(new function) evicting: %s %s\n", fout->function, vout->variable);
//						}
//						else
//							tmp_f_evictor=tmp_f_evictor->next;
//					}
//				} //while !fnc_set
//			}
//			else{
//        /* insert new function evictor */
//				vout->fnc_evictors = malloc(sizeof(evictor_ftype));
//				strcpy(vout->fnc_evictors->function, fin->function);
//        vout->fnc_evictors->num_f_evictions=1;
//        vout->fnc_evictors->next = NULL;
//				vout->fnc_evictors->next_v = malloc(sizeof(evictor_vtype));
//        vout->fnc_evictors->next_v->num_v_evictions=1;
//				strcpy(vout->fnc_evictors->next_v->variable, vin->variable);
//				//printf("(first function) evicting: %s %s\n", fout->function, vout->variable);
//			}
//    }
//	}
//	if(debug)
//		printf("update_conflicts END\n");
//	return;
//}

