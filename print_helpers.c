/* Various print helpers for cache statistics */

#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <sys/stat.h>

#include "d4.h"
static unsigned long counter = 0;

void create_dir(char* fullpath){
  char tmp[256];
	mkdir(fullpath, S_IRUSR | S_IWUSR | S_IXUSR | S_IXGRP | S_IWGRP | S_IRGRP);

  sprintf(tmp, "%s/functions", fullpath);
	mkdir(tmp, S_IRUSR | S_IWUSR | S_IXUSR | S_IXGRP | S_IWGRP | S_IRGRP);

  sprintf(tmp, "%s/structs", fullpath);
	mkdir(tmp, S_IRUSR | S_IWUSR | S_IXUSR | S_IXGRP | S_IWGRP | S_IRGRP);

	sprintf(tmp, "%s/segments", fullpath);
	mkdir(tmp, S_IRUSR | S_IWUSR | S_IXUSR | S_IXGRP | S_IWGRP | S_IRGRP);
}

static void _flush(FILE* fdf, FILE* fds, FILE* fdseg, d4cache* ctmp, unsigned int numsets, unsigned long refcount)
{
	//printf("flush(beg)\n");
	/* reset heads*/
	d4mltype* mltmp = ctmp->main_list;
	d4ftype* ftmp = mltmp->nextf;
	d4vtype* vtmp = NULL;
 	d4stype1* sitmp = mltmp->nexts;
	d4segstype* segtmp = ctmp->seginfo;

	/* print  cache set stats  */
	fprintf(fdf, "Refcount %lu\n", refcount);
	fprintf(fdf, "total_functions: %lu\n",mltmp->tmptfuncs);
	while(ftmp != NULL){
		//printf("flush(while(ftmp)) <%s> [%p]\n", ftmp->fname, ftmp);
		//fprintf(fdf, "%-28s %11s %11s %11s %6s\n", "Function", "Refs", "Hits", "Misses", "Vars"); 
		if(ftmp->tmptrefs != 0){
			fprintf(fdf, "%9s %-28s %11lu %11lu %11lu %6lu\n", "function: ", ftmp->fname, 
                                                   ftmp->tmptrefs, 
																									 ftmp->tmptrefs - ftmp->tmptmisses,
																									 ftmp->tmptmisses, ftmp->tmptvars);
			ftmp->tmptrefs = 0;
			ftmp->tmptmisses = 0;
			ftmp->tmptvars = 0;
			int i=0;
			for(i=0; i < numsets; i++){
				if(ftmp->sets[i].tmprefs != 0){
				fprintf(fdf, "%d %lu %lu %lu\n", i, ftmp->sets[i].tmprefs, ftmp->sets[i].tmprefs - ftmp->sets[i].tmpmisses
																 , ftmp->sets[i].tmpmisses);
				ftmp->sets[i].tmprefs = 0;
				ftmp->sets[i].tmpmisses = 0;
				}
			}
			
			vtmp = ftmp->variable;
			while(vtmp!=NULL){
				//printf("flush(while(vtmp)) <%s> [%p]\n", vtmp->vname, vtmp);
				fprintf(fdf, "variable: %s misses: %lu scope: %s\n", vtmp->vname, vtmp->tmptmisses, vtmp->scope);
				vtmp->tmptrefs = 0;
				vtmp->tmptmisses = 0;
				for(i=0; i < numsets; i++){
					if(vtmp->sets[i].tmprefs != 0){
						fprintf(fdf, "%d %lu %lu %lu\n", i, vtmp->sets[i].tmprefs, vtmp->sets[i].tmprefs - vtmp->sets[i].tmpmisses
																		 , vtmp->sets[i].tmpmisses);
						vtmp->sets[i].tmprefs = 0;
						vtmp->sets[i].tmpmisses = 0;
					}
				}
				vtmp = vtmp->next;
			}
		}
		ftmp = ftmp->next;
	}
	mltmp->tmptfuncs = 0;

	/* print structinfo cache set stats */
	sitmp = mltmp->nexts;
	fprintf(fds, "Refcount %lu\n", refcount);
	while(sitmp != NULL){
		//printf("flush(while(sitmp)) <%s> [%p]\n", sitmp->structure, sitmp);
		if(sitmp->tmpttrefs != 0){
			fprintf(fds, "structure: %s blocks: %lu\n", sitmp->sname, sitmp->tmpcurbcnts);
			int i = 0;
			//printf("       sitmp->bcnts: %lu\n", sitmp->bcnts);
			//printf("       sitmp->curbcnts: %lu\n", sitmp->curbcnts);
			for(i=0; i<sitmp->tmpbcnts; i++){
				if(sitmp->blocks[i].tmptrefs != 0){
					fprintf(fds, "H %d %lu %lu %lu\n", i, sitmp->blocks[i].tmptrefs, sitmp->blocks[i].tmptrefs - sitmp->blocks[i].tmptmisses
												 , sitmp->blocks[i].tmptmisses);
					sitmp->blocks[i].tmptrefs = 0;
					sitmp->blocks[i].tmptmisses = 0;
					int j = 0;
					for(j=0; j < numsets; j++){
						if(sitmp->blocks[i].sets[j].tmprefs != 0){
							fprintf(fds, "%d %lu %lu %lu\n", j, sitmp->blocks[i].sets[j].tmprefs, 
																					sitmp->blocks[i].sets[j].tmprefs - sitmp->blocks[i].sets[j].tmpmisses,
																					sitmp->blocks[i].sets[j].tmpmisses);
							sitmp->blocks[i].sets[j].tmprefs = 0;
							sitmp->blocks[i].sets[j].tmpmisses = 0;
						}
					}
				}
			}
			sitmp->tmpcurbcnts = 0;
			sitmp->tmpttrefs = 0;
			sitmp->tmpttmisses = 0;
		}
		sitmp = sitmp->next;
	}
	mltmp->tmptstructs = 0;
	
	/* print segment info */
	fprintf(fdseg,"%12lu %12lu %12lu %12lu %12lu\n", refcount, segtmp->stack[1], segtmp->heap[1], segtmp->global[1],
			                                       segtmp->stack[1] + segtmp->global[1] + segtmp->heap[1]);
	segtmp->stack[0] = 0;
	segtmp->stack[1] = 0;
	segtmp->global[0] = 0;
	segtmp->global[1] = 0;
	segtmp->heap[0] = 0;
	segtmp->heap[1] = 0;
  
	return;
}

void flush_and_clear(char* fullpath,char* outfile, d4cache* c, unsigned long refcount)
{
	d4cache* ctmp = c;

  char filename[256];
  FILE* fdf = NULL;
  FILE* fds = NULL;
  FILE* fdseg = NULL;

	while(ctmp->link != NULL){
		sprintf(filename,"%s/functions/%s.%s.function.snapshot.%lu", fullpath, ctmp->name, outfile, counter);
		fdf = fopen(filename, "wr");
		sprintf(filename,"%s/structs/%s.%s.struct.snapshot.%lu", fullpath, ctmp->name, outfile, counter);
		fds = fopen(filename, "wr");
		sprintf(filename,"%s/segments/%s.%s.segmisses", fullpath, ctmp->name, outfile);
		fdseg = fopen(filename, "a");
		

		if(fdf == NULL || fds == NULL){
			printf("error opening file for flush, exiting!\n");
			exit(1);
		}

		_flush(fdf, fds, fdseg, ctmp, D4VAL(ctmp, numsets), refcount);
		
		fclose(fdf);
		fclose(fds);
		fclose(fdseg);
		ctmp = ctmp->link;	
	}	
	counter++;
  
  return;
}
	
