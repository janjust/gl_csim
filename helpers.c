/* various sorting functions */

#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "d4.h"

void sortvtype(d4ftype* ftmp)
{
	int i = 0, j = 0, tvars;
	d4vtype* vtmp = NULL;
	d4vtype* tmp = NULL;
	d4vtype** sorttmp;	
	
	while(ftmp != NULL){
		vtmp = ftmp->variable;
		tvars = ftmp->tvars;
		
		if(tvars < 2){
			ftmp = ftmp->next;
			continue;
		}
		sorttmp = calloc(tvars, sizeof(d4vtype*));
		/* flatten */
		for(i=0; i<tvars; i++){
			sorttmp[i] = vtmp;
			vtmp=vtmp->next;
		}

		/* sort */
		for(i=0; i<tvars-1; i++){
			for(j=0; j<tvars-1; j++){
				if(sorttmp[j]->tmisses < sorttmp[j+1]->tmisses){
					tmp = sorttmp[j];
					sorttmp[j] = sorttmp[j+1];
					sorttmp[j+1] = tmp;
				}
			}
		}

		/* connect */
		ftmp->variable = sorttmp[0];
		for(i=0; i<tvars-1; i++){
			sorttmp[i]->next = sorttmp[i+1];
		}
		sorttmp[i]->next = NULL;
		
		free(sorttmp);
		ftmp = ftmp->next;
	}
	
	return;
}

d4ftype* sortftype(d4mltype* mltmp)
{
	d4ftype* ftmp = mltmp->nextf;	
	int i = 0, j = 0;
	int tfncts = mltmp->tfuncs;
	d4ftype** sorttmp = calloc(tfncts, sizeof(d4ftype*));

	/* flatten */
	for(i=0; i<tfncts; i++){
		sorttmp[i] = ftmp;
		ftmp = ftmp->next;
	}

	/* sort */
	d4ftype* tmp;
	for(i=3; i<tfncts-1; i++){
		for(j=3; j<tfncts-1; j++){
			if(sorttmp[j]->tmisses < sorttmp[j+1]->tmisses){
				tmp = sorttmp[j];
				sorttmp[j] = sorttmp[j+1];
				sorttmp[j+1] = tmp;
			}
		}
	}

	/* connect */
	ftmp = sorttmp[0];
	for(i=0; i<tfncts-1; i++){
		sorttmp[i]->next = sorttmp[i+1];
	}
	sorttmp[i]->next = NULL;

	free(sorttmp);
	return ftmp;
}

d4stype1* sortsitype(d4mltype* mltype)
{
	d4stype1* sitmp = mltype->nexts;
	if(sitmp == NULL){
		return sitmp;
	}

	int i = 0, j = 0;
	int tstructs = mltype->tstructs;
	d4stype1** sorttmp = calloc(tstructs, sizeof(d4stype1*));
		
	/* flatten */
	for(i=0; i<tstructs; i++){
		sorttmp[i] = sitmp;
		sitmp = sitmp->next;
	}

	/* sort */
	d4stype1* tmp;
	for(i=0; i<tstructs-1; i++){
		for(j=0; j<tstructs-1; j++){
			if(sorttmp[j]->ttmisses < sorttmp[j+1]->ttmisses){
				tmp = sorttmp[j];
				sorttmp[j] = sorttmp[j+1];
				sorttmp[j+1] = tmp;
			}
		}
	}

	/* connect */
	sitmp = sorttmp[0];
	for(i = 0; i<tstructs-1; i++){
		sorttmp[i]->next = sorttmp[i+1];
	}
	sorttmp[i]->next = NULL;

	free(sorttmp);

	return sitmp;
}

void move_ftype_1up ( d4mltype* mltmp, d4ftype* ftmp, char* fname)
{
	int i = 0;
	unsigned long llen = 0;
	d4ftype** flisttmp = NULL;	
	llen = mltmp->tfuncs;

	if(mltmp->flist == NULL){
		mltmp->flist = calloc(mltmp->tfuncs, sizeof(d4ftype*));
		mltmp->llen = mltmp->tfuncs;
		
		ftmp = mltmp->nextf;
		for(i = 0; ftmp != NULL; i++){
			mltmp->flist[i] = ftmp;
			ftmp = ftmp->next;
		}
		mltmp->flist[i] = NULL;

		return;
	}
	else if(mltmp->tfuncs > mltmp->llen){
		mltmp->llen = mltmp->tfuncs;
		
		/* realloc memory*/
		flisttmp = mltmp->flist;
		mltmp->flist = realloc(flisttmp, llen * sizeof(d4ftype*));
		mltmp->flist[mltmp->llen-1] = ftmp;

		/*move last pos. 1 up*/
		ftmp = mltmp->flist[mltmp->llen-2];
		mltmp->flist[mltmp->llen-2] = mltmp->flist[mltmp->llen-1];
		mltmp->flist[mltmp->llen-1] = ftmp;

		/* connect */
		mltmp->flist[llen-3]->next = mltmp->flist[llen-2];
		mltmp->flist[llen-2]->next = mltmp->flist[llen-1];
		mltmp->flist[mltmp->llen-1]->next = NULL;

		return;
	}

	if(strcmp(mltmp->flist[3]->fname, fname) == 0){
		/* do nothing, it's already on top */
		return;
	}
		
	/* move 1 up */
	for(i = 3; i < llen; i++){
		if(strcmp(mltmp->flist[i]->fname, fname) == 0){
			
			/* swap and connect*/
			ftmp = mltmp->flist[i];
			mltmp->flist[i] = mltmp->flist[i-1];
			mltmp->flist[i-1] = ftmp;

			mltmp->flist[i-2]->next = mltmp->flist[i-1];
			mltmp->flist[i-1]->next = mltmp->flist[i];
			
			if(i < (llen-1)){
				mltmp->flist[i]->next = mltmp->flist[i+1];
			}
			else{
				mltmp->flist[i]->next = NULL;
			}
		}
	}

	return;
}
